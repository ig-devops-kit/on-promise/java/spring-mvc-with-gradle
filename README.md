# Spring MVC with Gradle
Spring MVC - Web Demo Gradle CI/CD 파이프라인 예제
#### 환경
- Java version 1.8
- Spring version 3.2.11
- Servlet version 3.1.0
- SLF4J version 1.7.13
- Logback version 1.1.2
- Junit version 4.12



#### 파이프라인 정보
- **Gradle Basic** 
  <!-- [gradle:build, gradle:test] -->
- **Spring Boot Gradle with SSH JAR** 
  <!-- [gradle:build, gradle:test, security:semgrep-sast, security:code-quality, security:secret_detection, deploy:jar] -->
- **Spring Boot Gradle with Docker Compose**
  <!-- [gradle:build, gradle:test, security:semgrep-sast, security:code-quality, security:secret_detection, security:container_scanning, docker:kaniko, deploy:docker-compose] -->