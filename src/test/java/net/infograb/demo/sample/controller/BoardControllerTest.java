package net.infograb.demo.sample.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-sample-servlet.xml")
@WebAppConfiguration
public class BoardControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testBoardListPage() throws Exception {
        Date regDate1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-02");
        Date regDate2 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-03");

        this.mockMvc.perform(get("/board/list.do"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(view().name("board/list"))
                .andExpect(forwardedUrl("/WEB-INF/views/board/list.jsp"))
                .andExpect(model().attribute("boards", hasSize(2)))
                .andExpect(model().attribute("boards", hasItem(
                        allOf(
                                hasProperty("id", is(1)),
                                hasProperty("title", is("첫 번째 글의 제목")),
                                hasProperty("contents", is("첫 번째 글의 내용")),
                                hasProperty("viewCnt", is(5)),
                                hasProperty("author", is("작성자1")),
                                hasProperty("regDate", is(regDate1))
                        )
                )))
                .andExpect(model().attribute("boards", hasItem(
                        allOf(
                                hasProperty("id", is(2)),
                                hasProperty("title", is("두 번째 글의 제목")),
                                hasProperty("contents", is("두 번째 글의 내용")),
                                hasProperty("viewCnt", is(3)),
                                hasProperty("author", is("작성자2")),
                                hasProperty("regDate", is(regDate2))
                        )
                )));
    }

}
