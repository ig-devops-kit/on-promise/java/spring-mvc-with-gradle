package net.infograb.demo.sample.service;

import net.infograb.demo.sample.common.BaseService;
import net.infograb.demo.sample.model.Board;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BoardService extends BaseService {

    public List<Board> retrieveBoards() throws Exception {
        List<Board> boards = new ArrayList<Board>();

        Date regDate1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-02");
        Board board1 = buildBoard(1, "첫 번째 글의 제목", "첫 번째 글의 내용", 5, "작성자1", regDate1);

        Date regDate2 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-03");
        Board board2 = buildBoard(2, "두 번째 글의 제목", "두 번째 글의 내용", 3, "작성자2", regDate2);

        boards.add(board2);
        boards.add(board1);

        return boards;
    }

    private Board buildBoard(int id, String title, String contents, int viewCnt, String author, Date regDate) {
        Board board = new Board();
        board.setId(id);
        board.setTitle(title);
        board.setContents(contents);
        board.setViewCnt(viewCnt);
        board.setAuthor(author);
        board.setRegDate(regDate);

        return board;
    }

}
