package net.infograb.demo.sample.controller;

import net.infograb.demo.sample.common.BaseController;
import net.infograb.demo.sample.model.Board;
import net.infograb.demo.sample.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/board")
public class BoardController extends BaseController {

    @Autowired
    private BoardService boardService;

    @RequestMapping(value = "/list.do")
    public String list(ModelMap model) throws Exception {
        List<Board> boards = boardService.retrieveBoards();

        model.addAttribute("boards", boards);

        return "board/list";
    }

}
